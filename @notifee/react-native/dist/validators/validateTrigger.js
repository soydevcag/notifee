"use strict";
/*
 * Copyright (c) 2016-present Invertase Limited
 */
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
const Trigger_1 = require("../types/Trigger");
const MINIMUM_INTERVAL = 15;
function isMinimumInterval(interval, timeUnit) {
    switch (timeUnit) {
        case Trigger_1.TimeUnit.SECONDS:
            return interval / 60 >= MINIMUM_INTERVAL;
        case Trigger_1.TimeUnit.MINUTES:
            return interval >= MINIMUM_INTERVAL;
        case Trigger_1.TimeUnit.HOURS:
            return interval >= 1;
        case Trigger_1.TimeUnit.DAYS:
            return interval >= 1;
    }
    return true;
}
function validateTrigger(trigger) {
    if (!utils_1.isObject(trigger)) {
        throw new Error("'trigger' expected an object value.");
    }
    switch (trigger.type) {
        case Trigger_1.TriggerType.TIMESTAMP:
            return validateTimestampTrigger(trigger);
        case Trigger_1.TriggerType.INTERVAL:
            return validateIntervalTrigger(trigger);
        default:
            throw new Error('Unknown trigger type');
    }
}
exports.default = validateTrigger;
function validateTimestampTrigger(trigger) {
    if (!utils_1.isNumber(trigger.timestamp)) {
        throw new Error("'trigger.timestamp' expected a number value.");
    }
    const now = Date.now();
    if (trigger.timestamp <= now) {
        throw new Error("'trigger.timestamp' date must be in the future.");
    }
    const out = {
        type: trigger.type,
        timestamp: trigger.timestamp,
        repeatFrequency: -1,
    };
    if (utils_1.objectHasProperty(trigger, 'repeatFrequency')) {
        if (!utils_1.isValidEnum(trigger.repeatFrequency, Trigger_1.RepeatFrequency)) {
            throw new Error("'trigger.repeatFrequency' expected a RepeatFrequency value.");
        }
        out.repeatFrequency = trigger.repeatFrequency;
    }
    return out;
}
function validateIntervalTrigger(trigger) {
    if (!utils_1.isNumber(trigger.interval)) {
        throw new Error("'trigger.interval' expected a number value.");
    }
    const out = {
        type: trigger.type,
        interval: trigger.interval,
        timeUnit: Trigger_1.TimeUnit.SECONDS,
    };
    if (utils_1.objectHasProperty(trigger, 'timeUnit')) {
        if (!utils_1.isValidEnum(trigger.timeUnit, Trigger_1.TimeUnit)) {
            throw new Error("'trigger.timeUnit' expected a TimeUnit value.");
        }
        out.timeUnit = trigger.timeUnit;
    }
    if (!isMinimumInterval(trigger.interval, out.timeUnit)) {
        throw new Error("'trigger.interval' expected to be at least 15 minutes.");
    }
    return out;
}
//# sourceMappingURL=validateTrigger.js.map