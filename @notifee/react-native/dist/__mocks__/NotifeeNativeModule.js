"use strict";
/**
 * @format
 */
/* eslint-env jest */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mockNotifeeNativeModule = void 0;
const NotifeeJSEventEmitter_1 = __importDefault(require("../NotifeeJSEventEmitter"));
exports.mockNotifeeNativeModule = {
    getTriggerNotificationIds: jest.fn(),
    cancelAllNotifications: jest.fn(),
    cancelDisplayedNotifications: jest.fn(),
    cancelTriggerNotifications: jest.fn(),
    cancelNotification: jest.fn(),
    cancelDisplayedNotification: jest.fn(),
    cancelTriggerNotification: jest.fn(),
    createChannel: jest.fn(),
    createChannels: jest.fn(),
    createChannelGroup: jest.fn(),
    createChannelGroups: jest.fn(),
    deleteChannel: jest.fn(),
    deleteChannelGroup: jest.fn(),
    displayNotification: jest.fn(),
    createTriggerNotification: jest.fn(),
    getChannel: jest.fn(),
    getChannels: jest.fn(),
    getChannelGroup: jest.fn(),
    getChannelGroups: jest.fn(),
    getInitialNotification: jest.fn(),
    onBackgroundEvent: jest.fn(),
    onForegroundEvent: jest.fn(),
    openNotificationSettings: jest.fn(),
    requestPermission: jest.fn(),
    registerForegroundService: jest.fn(),
    setNotificationCategories: jest.fn(),
    getNotificationCategories: jest.fn(),
    getNotificationSettings: jest.fn(),
    getBadgeCount: jest.fn(),
    setBadgeCount: jest.fn(),
    incrementBadgeCount: jest.fn(),
    decrementBadgeCount: jest.fn(),
    isBatteryOptimizationEnabled: jest.fn(),
    openBatteryOptimizationSettings: jest.fn(),
    getPowerManagerInfo: jest.fn(),
    openPowerManagerSettings: jest.fn(),
    stopForegroundService: jest.fn(),
    hideNotificationDrawer: jest.fn(),
};
const mock = jest.fn().mockImplementation(() => {
    return { emitter: NotifeeJSEventEmitter_1.default, native: exports.mockNotifeeNativeModule };
});
exports.default = mock;
//# sourceMappingURL=NotifeeNativeModule.js.map