/**
 * @format
 */
/// <reference types="jest" />
export declare const mockNotifeeNativeModule: {
    getTriggerNotificationIds: jest.Mock<any, any>;
    cancelAllNotifications: jest.Mock<any, any>;
    cancelDisplayedNotifications: jest.Mock<any, any>;
    cancelTriggerNotifications: jest.Mock<any, any>;
    cancelNotification: jest.Mock<any, any>;
    cancelDisplayedNotification: jest.Mock<any, any>;
    cancelTriggerNotification: jest.Mock<any, any>;
    createChannel: jest.Mock<any, any>;
    createChannels: jest.Mock<any, any>;
    createChannelGroup: jest.Mock<any, any>;
    createChannelGroups: jest.Mock<any, any>;
    deleteChannel: jest.Mock<any, any>;
    deleteChannelGroup: jest.Mock<any, any>;
    displayNotification: jest.Mock<any, any>;
    createTriggerNotification: jest.Mock<any, any>;
    getChannel: jest.Mock<any, any>;
    getChannels: jest.Mock<any, any>;
    getChannelGroup: jest.Mock<any, any>;
    getChannelGroups: jest.Mock<any, any>;
    getInitialNotification: jest.Mock<any, any>;
    onBackgroundEvent: jest.Mock<any, any>;
    onForegroundEvent: jest.Mock<any, any>;
    openNotificationSettings: jest.Mock<any, any>;
    requestPermission: jest.Mock<any, any>;
    registerForegroundService: jest.Mock<any, any>;
    setNotificationCategories: jest.Mock<any, any>;
    getNotificationCategories: jest.Mock<any, any>;
    getNotificationSettings: jest.Mock<any, any>;
    getBadgeCount: jest.Mock<any, any>;
    setBadgeCount: jest.Mock<any, any>;
    incrementBadgeCount: jest.Mock<any, any>;
    decrementBadgeCount: jest.Mock<any, any>;
    isBatteryOptimizationEnabled: jest.Mock<any, any>;
    openBatteryOptimizationSettings: jest.Mock<any, any>;
    getPowerManagerInfo: jest.Mock<any, any>;
    openPowerManagerSettings: jest.Mock<any, any>;
    stopForegroundService: jest.Mock<any, any>;
    hideNotificationDrawer: jest.Mock<any, any>;
};
declare const mock: jest.Mock<any, any>;
export default mock;
